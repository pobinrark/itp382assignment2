﻿using UnityEngine;
using System.Collections;

public class Apple : MonoBehaviour {
	public static float bottomY = -20f;
	public GUIText scoreGT;
	private int currentScore = 0;
	// Use this for initialization
	void Start () {
	
	}


	
	// Update is called once per frame
	void Update () {
		if ( transform.position.y < bottomY ) {
			Destroy( this.gameObject );
			// Get a reference to the ApplePicker component of Main Camera
			ApplePicker apScript = Camera.main.GetComponent<ApplePicker>(); // 1
			// Call the public AppleDestroyed() method of apScript
			apScript.AppleDestroyed(); 
		}
	}
	//1a. moved OnCollisionEnter to apple and change the collided with tag to Basket
	//then make sure to add the basket tag to basket.
	//1b. move the gui text to apple not basket
	/*void OnCollisionEnter( Collision coll ) {                               // 2
		// Find out what hit this basket
		GameObject collidedWith = coll.gameObject;                          // 3
		if ( collidedWith.tag == "Basket" ) {                                // 4
			Destroy( collidedWith );
		}
		// Parse the text of the scoreGT into an int
		int score = int.Parse( scoreGT.text );                              // 4
		// Add points for catching the apple
		score += 100;
		// Convert the score back to a string and display it
		scoreGT.text = score.ToString();

		if (score > currentScore){
			currentScore = score;
		}
		// Track the high score
		if (score > HighScore.score) {
			HighScore.score = score;
		}
	}*/
}
